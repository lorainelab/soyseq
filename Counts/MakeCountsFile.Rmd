Processing counts data - single-mapping reads
========================================================

Introduction
------------
This file contains code for processing raw counts data output by countReadsWithSamtools.py. The script countReadsWithSamtools.py reads a file of gene model annotations, calculates the extent of the gene by taking the smallest start and the largest end of all the gene models belonging to that gene, and then uses samtools to (`samtools view -c`) to get the number of reads per region. It also assumes that gene models with the same prefix come from the same gene. For example, Glyma01g00320.1 and Glyma01g00320.2 are gene models representing alternative transcripts transcribed from gene Glyma01g00320.

Gene model annotations used were obtained from the Phytozome ftp Web site on June 24 and converted to BED format. For details, see genomes subversion repo for IGBQuickLoad.

### Overview of the experiment

The samples were from three genotypes, three plants per genotype, and three soybeans per plant. Thus, we have 36 samples, nine samples per genotype. 

Data processing
---------------

Alignments were from running tophat 2.0.6, using the soybean genome assembly from August 2010. Aligned reads per gene were counted using countReadsWithSamtools.py.

```{r}
fname='data/G_max_TH2.0.6_reads.csv.gz'
d=read.csv(fname,header=F)
headerrows=which(d[,1]=='gene')
d=d[-headerrows,]
names(d)=c('gene','sample','count')
d$count=as.numeric(as.character(d$count))
```

The data are in long form, with the name of the BAM file in the second column. The BAM file names correspond to samples. 

Reshape the data so that samples are columns and genes are rows:

```{r}
suppressPackageStartupMessages(library(reshape))
d=cast(d,gene~sample) 
```

Modify variable names so that they don't include the file extensions "sm.bam". Change samples named 764 to E764, since R doesn't like variable names that start with numbers.

```{r}
newnames=sub('.sm.bam','',names(d))
newnames=sub('764','E764',newnames)
names(d)=newnames
```

Make tab-separated value (tsv) data file. 

```{r}
fname='results/SoySeq_counts.tsv'
write.table(d,file=fname,sep='\t',row.names=F,quote=F)
system(paste('gzip -f',fname))
```



