#!/bin/bash
# featureCounts v1.4.5
# /lustre/groups/lorainelab/sw/subread-1.4.5-Linux-x86_64/bin/featureCounts
# https://bitbucket.org/lorainelab/soyseq
SAF=$HOME/src/soyseq/GeneRegions/results/SAF_for_featureCount.tsv
BASE=counts_mm
OUT=$BASE.tsv
ERR=$BASE.err
OUT2=$BASE.out
# include multi-mapping reads
featureCounts -M -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
BASE=counts_sm
OUT=$BASE.tsv
ERR=$BASE.err
OUT2=$BASE.out
# don't include multi-mapping reads
featureCounts -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
BASE=counts_pm
OUT=$BASE.tsv
ERR=$BASE.err
OUT2=$BASE.out
# use primary alignment only
featureCounts --primary -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2


