# Scripts used to run cufflinks suite

These include:

* run_map275.sh - align reads onto genome
* run_cuff.sh - assemble alignments into transcript models
* run_merge.sh - merge outputs from previous steps
* run_big_diff_275.sh - make files for differential expression analysis

**Note:** Scripts are here for documentation purposes only. They assume files reside in locations that probably don't exist on your file system. 

# Questions?

Contact:

* Adam Whaley awhaley9@uncc.edu

