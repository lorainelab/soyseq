# Analysis of insert expression

The three "whole plasmid" fasta files in the data/ directory were exported as fastas from snapgene.

Used src/makeFakeChr.py to create contig containing 
conactenated transgene sequences separated by N characters.

Re-ran alignment software using soybean genome assembly
with fake contig added. 

Counted reads aligned to transgenes using countT.sh.

Results are in:

* results/insert_counts.tsv.gz

Sequence files are in:

* results/chrT.fa

BED file annotations are in:

* results/chrT.bed

Original sequence files are in data. 

## Questions? ##

Contact:

* Ann Loraine aloraine@uncc.edu
