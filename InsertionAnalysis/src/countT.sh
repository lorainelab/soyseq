#!/bin/bash
# run like this;
# ls *.bam | xargs -I FILE countTransgene.sh FILE > chrT-hTG-ST111-T764.csv
FILE=$1
echo 'gene,bam,count'

L=chrT-hTG-ST111-T764
T=`samtools view -c $FILE $L`
G='chrT-hTG-ST111-T764'
echo "$G,$FILE,$T"

L1="100-8439"
T1=`samtools view -c $FILE $L:$L1`
G1=hTG
echo "$G1,$FILE,$T1"

L2="8539-10590"
T2=`samtools view -c $FILE $L:$L2`
G2=ST111
echo "$G2,$FILE,$T2"

L3="10690-11523"
T3=`samtools view -c $FILE $L:$L3`
G3=T764
echo "$G3,$FILE,$T3"



