#!/usr/bin/env python

ex=\
"""
Read sequences from fasta files from stdin.
Use the sequence to make a fake contig containing
the sequences with N's between them. Also write
a BED file indicate location of sequences.
Write fasta to stdout.
Write BED file to -b.
"""

import sys,os,optparse,gzip
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import Mapping.FeatureModel as F
import Mapping.Parser.Bed as b

def readSeqs():
    handle = sys.stdin
    record_dict=SeqIO.to_dict(SeqIO.parse(handle,'fasta'))
    return record_dict

def main(options,args):
    d = readSeqs()
    sys.stderr.write("Got %i records.\n"%len(d.keys()))
    bigstring='N'*100 # not sure if this is the right amount, this will space them out nicely probably
    bigseq = bigstring
    chrName = 'chrT'
    feats=[]
    for (name,record) in d.items():
        sys.stderr.write('writing %s length %i\n'%(name,len(record.seq)))
        start=len(bigseq)
        bigseq=bigseq+str(record.seq)+bigstring
        chrName=chrName+'-'+name
        cfeat=F.CompoundDNASeqFeature(seqname=chrName,
                                      display_id=name,
                                      feat_type='mRNA',
                                      start=start,
                                      length=len(str(record.seq)),
                                      strand=1)
        exon=F.DNASeqFeature(seqname=chrName,
                             feat_type='exon',
                             start=start,
                             length=len(str(record.seq)),
                             strand=1)
        cfeat.addFeat(exon)
        feats.append(cfeat)
    for feat in feats:
        feat.setSeqname(chrName)
    record=SeqRecord(Seq(bigseq),
                     id=chrName,
                     description='')
    SeqIO.write(record,sys.stdout,'fasta')
    if options.bed_file:
        b.feats2bed(fname=options.bed_file,feats=feats)

if __name__ == '__main__':
    usage = "%prog -b FILE.bed > out.fa\n"+ex
    parser = optparse.OptionParser(usage)
    parser.add_option('-b','--bed_file',dest='bed_file',
                      help='Write bed file with fake gene coordinates.',
                      metavar='BED_FILE',default=None)
    (options,args)=parser.parse_args()
    main(options,args)



