# Scripts 

This directory contains shell scripts (mostly), R code, and python scripts
we used to generate data files for analysis or anlayze data. 

Mostly we ran scripts on a compute cluster nicknamed "Halcyon." 

* * * 

## What's here

* runTopHat.sh - job script for aligning reads onto soybean genome, transcriptome
* align.sh - submits runTopHat.sh jobs to cluster
* Common.R - functions used by R code throughout the repository

