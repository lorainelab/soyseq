#!/bin/bash
# run like this: 
# assemble.sh 1>jobs.out 2>jobs.err
# then if you have to kill the jobs, 
# cat jobs.out | xargs qdel 
SCRIPT=runCufflinks.sh
D=${1:-"soyseq_cuff2.2.1"}
G=${2:-"G_max_Jan_2014_plusT.fa"}
FS=`ls *.bam`
T=Gmax_275_Wm82.a2.v1.gene_exons.gff3
for F in $FS
do
    S=`basename $F .bam`
    CMD="qsub -N $S -o $S.out -e $S.err -vSAMPLE=$S,ANNOTATIONS=$T,OUTDIR=$D,GENOMEFASTA=$G $SCRIPT"
    $CMD
done
