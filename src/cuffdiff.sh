#!/bin/bash
SCRIPT=runCuffdiff.sh
G=${1:-"G_max_Jan_2014_plusT.fa"}
T=${2:-"merged.gtf"}
O=${3:-"cuffdiff2.2.1"}
TGS="ST77 ST111 764"
for S in $TGS
do
    CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,T=$T,G=$G,O=$O $SCRIPT"
    $CMD
done
