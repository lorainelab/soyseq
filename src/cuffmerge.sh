#!/bin/bash
D=${1:-"soyseq_cuff2.2.1"}
G=${2:-"G_max_Jan_2014_plusT.fa"}
T=${3:-"Gmax_275_Wm82.a2.v1.gene_exons.gff3"}
#find ./$D -name transcripts.gtf -print > assemblies.txt
cuffmerge -g $T -s $G -p 8 assemblies.txt