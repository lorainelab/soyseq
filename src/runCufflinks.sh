#!/bin/bash
#PBS -l nodes=1:ppn=4
#PBS -l vmem=32000mb
#PBS -l walltime=20:00:00
cd $PBS_O_WORKDIR
# OUTDIR, SAMPLE, GENOME, ANNOTS defined by qsub -v 
echo "OUTDIR: $OUTDIR"
echo "SAMPLE: $SAMPLE"
echo "GENOMEFASTA: $GENOMEFASTA"
echo "ANNOTATIONS: $ANNOTATIONS"
cufflinks -u -g $ANNOTATIONS -o $OUTDIR/$SAMPLE -p 4 -b $GENOMEFASTA $SAMPLE.bam
