#!/bin/bash
#PBS -l nodes=1:ppn=4
#PBS -l vmem=32000mb
#PBS -l walltime=20:00:00
cd $PBS_O_WORKDIR
# OUTDIR, SAMPLE, GENOME, FASTQ, TRANS defined by qsub -v 
echo "OUTDIR: $OUTDIR"
echo "SAMPLE: $SAMPLE"
echo "GENOME: $GENOME"
echo "FASTQ: $FASTQ"
echo "TRANS: $TRANS"
# assumes transcriptome index already made, see 
# http://ccb.jhu.edu/software/tophat/manual.shtml
tophat -p 4 -I 5000 --transcriptome-index $TRANS -o $OUTDIR/$SAMPLE $GENOME $FASTQ
