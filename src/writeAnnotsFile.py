#!/usr/bin/env python

"""Creating annotation file for soybean."""

import re,sys,os,gzip

def getGenes(fname='G_max_Aug_2010.bed.gz'):
    d={}
    fh=gzip.GzipFile(fname)
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        loc_id=line.split('\t')[3].split('.')[0]
        if not loc_id.startswith('Glyma'):
            raise ValueError("Unrecognizable id: %s from %s"%(loc_id,line.rstrip()))
        d[loc_id]=1
    return d

def getAnnots(fname='Glyma1.0-annotations.tsv'):
    fh = open(fname)
    d={}
    agireg=re.compile(r'AT[1-5CM]G\d{5}')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks=line.split('\t')
        if not len(toks)==11:
            continue
        loc_id=toks[1]
        if toks[4]=='AT':
            val=toks[7]
            atgene=toks[6]
            if not agireg.search(atgene):
                raise ValueError("AGI code %s not right for %s on line %s"%(atgene,loc_id,line.rstrip()))
            # clean up val
            newval=' | '.join(val.split(' | ')[0:-1])
            newval=newval+' '+atgene
            if d.has_key(loc_id):
                if d[loc_id]!=newval:
                    raise ValueError("Descriptions don't match for %s"%loc_id)
            else:
                d[loc_id]=newval
    return d

def writeGeneInfo(outfn='annotations.tsv',annots=None):
    fh=open(outfn,'w')
    line='gene\tdescr\n'
    fh.write(line)
    for loc_id in annots.keys():
        line='%s\t%s\n'%(loc_id,annots[loc_id])
        fh.write(line)
    fh.close()

def main():
    annots=getAnnots()
    writeGeneInfo(annots=annots)

if __name__ == '__main__':
    main()
